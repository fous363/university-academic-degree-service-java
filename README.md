# University-Academic-Degree-Service

This is an academic degree service project.

Endpoints:

- GET /api/academicDegree - get all academic degrees
- GET /api/academicDegree/{id} - get academic degree by id{id}
- POST /api/academicDegree - create academic degree
- PUT /api/academicDegree - update academic degree
- DELETE /api/academicDegree/{id} - delete academic degree

For local environment run:

- Update the application-dev.yml 
- Run "gradle bootRun --args="--spring.profiles.active=dev""

For production environment run:

- Update application-prod.yml
- Build from Dockerfile