package kz.zhanbolat.academicdegree.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "academic_degree")
public class AcademicDegree {
    /**
     * The identifier of a academic degree
     */
    @Id
    @GeneratedValue
    private Integer id;
    /**
     * The name of a academic degree
     */
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AcademicDegree that = (AcademicDegree) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "AcademicDegree{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
