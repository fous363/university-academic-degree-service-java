package kz.zhanbolat.academicdegree.controller;

import kz.zhanbolat.academicdegree.entity.AcademicDegree;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Rest repository controller for academic degree
 *
 * REQUEST MAPPING: /api/academicDegree
 */
@RepositoryRestResource(path = "academicDegree", collectionResourceRel = "academicDegree")
public interface AcademicDegreeRepository extends CrudRepository<AcademicDegree, Integer> {
}
