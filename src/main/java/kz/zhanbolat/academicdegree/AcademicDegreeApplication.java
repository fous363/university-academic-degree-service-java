package kz.zhanbolat.academicdegree;

import kz.zhanbolat.academicdegree.entity.AcademicDegree;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

@SpringBootApplication
@EntityScan("kz.zhanbolat.academicdegree")
public class AcademicDegreeApplication implements RepositoryRestConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(AcademicDegreeApplication.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config, CorsRegistry cors) {
		config.exposeIdsFor(AcademicDegree.class);
	}
}
