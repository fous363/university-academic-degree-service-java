package kz.zhanbolat.academicdegree;

import kz.zhanbolat.academicdegree.controller.filter.AuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthorizationFilter authorizationFilter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.formLogin().disable().logout().disable().csrf().disable()
                .authorizeRequests(authorizeRequestsCustomizer -> authorizeRequestsCustomizer
                        .antMatchers(HttpMethod.GET, "/api/academicDegree/**").permitAll()
                        .antMatchers(HttpMethod.POST, "/api/academicDegree/**").hasAuthority("ADMIN")
                        .antMatchers(HttpMethod.PUT, "/api/academicDegree/**").hasAuthority("ADMIN")
                        .and()
                        .addFilterBefore(jwtAuthorizationFilterBean().getFilter(), UsernamePasswordAuthenticationFilter.class))
                .cors();
    }

    @Bean
    public FilterRegistrationBean jwtAuthorizationFilterBean() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(authorizationFilter);
        registration.addUrlPatterns("/api/academicDegree**");
        registration.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return registration;
    }
}
