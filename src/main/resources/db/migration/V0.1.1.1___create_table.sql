create table academic_degree(
    id smallserial primary key,
    name varchar(100) not null
);